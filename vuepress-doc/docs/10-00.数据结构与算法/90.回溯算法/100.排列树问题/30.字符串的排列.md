---
title: 字符串的排列
date: 2022-09-17 12:21:26
permalink: /pages/2bcab7/
---
## 📃 题目描述

题目链接：[BM58 字符串的排列](https://www.nowcoder.com/practice/fe6b651b66ae47d7acce78ffdd9a96c7?tpId=295&tqId=23291&ru=/exam/oj&qru=/ta/format-top101/question-ranking&sourceUrl=%2Fexam%2Foj)

![](C:\Users\19124\AppData\Roaming\Typora\typora-user-images\image-20220917122203090.png)

## 🔔 解题思路

和 [剑指 Offer II 084. 含有重复元素集合的全排列 - 力扣（LeetCode） (leetcode-cn.com)](https://leetcode-cn.com/problems/7p8L0Z/) 差不多，这里就当练习回顾一下


```java
import java.util.*;
public class Solution {
    ArrayList<String> res = new ArrayList<>();
    public ArrayList<String> Permutation(String str) {
        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        boolean[] used = new boolean[chars.length];
        StringBuilder sb = new StringBuilder();
        backtrack(chars, used, sb);
        return res;
    }

    private void backtrack(char[] chars, boolean[] used, StringBuilder sb) {
        if (sb.length() == chars.length) {
            res.add(new String(sb));
            return ;
        }

        for (int i = 0; i < chars.length; i ++) {
            if (used[i] || (i != 0 && chars[i] == chars[i - 1] && !used[i - 1])) {
                continue;
            }

            sb.append(chars[i]);
            used[i] = true;
            backtrack(chars, used, sb);
            sb.deleteCharAt(sb.length() - 1);
            used[i] = false;
        }
    }
}
```

## 💥 复杂度分析

- 空间复杂度：
- 时间复杂度：

